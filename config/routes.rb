Rails.application.routes.draw do

	get 'sessions/new'

	root 'static_pages#home'
	
	get '/help', to: 'static_pages#help'
	get '/about', to: 'static_pages#about'
	get '/contact', to: 'static_pages#contact'

	get    '/login',   to: 'sessions#new'
	post   '/login',   to: 'sessions#create'
	delete '/logout',  to: 'sessions#destroy'

	get '/register', to: 'users#new'
	post '/register',  to: 'users#create'
	resources :users

	resources :microposts, only: [:create, :destroy]

end